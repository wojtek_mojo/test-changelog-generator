# Changelog

## Unreleased (2021-12-30)

#### New Features

* a new commit added

Full set of changes: [`0.1.0...8a1bf38`](https://gitlab.com/wojtek_mojo/test-changelog-generator/-/compare/0.1.0...8a1bf38)

## 0.1.0 (2021-12-28)

